import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  lorem = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque magnam tempore rem quis quia necessitatibus, perspiciatis, distinctio obcaecati quaerat debitis vitae doloremque iure repudiandae minima asperiores ullam, quod temporibus! Illo?';
  constructor() { }

  ngOnInit(): void {
  }

}
